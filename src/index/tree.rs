use crate::{
    error::{PERes, PIRes},
    index::{
        config::IndexOrd,
        keeper::{IndexKeeper, IndexLimits, IndexModify},
        tree::{
            key_changes::KeyChanges,
            lock_group::{LockGroup, LockGroups},
            nodes::{compare, Leaf, Node, PageIter, PageIterBack, TreeNode, TreeNodeRef, Value},
            parent_change::{ChildChanged, ParentChange},
        },
    },
};
use std::{cmp::Ordering, fmt::Display, ops::Bound, rc::Rc};

use self::path::{BottomDepth, Path, TopDepth};
use super::config::IndexTypeInternal;

mod lock_group;
pub mod nodes;
mod parent_change;
mod path;

#[cfg(test)]
mod nodes_tests;
#[cfg(test)]
mod tests;

pub(crate) mod key_changes;

pub(crate) struct ChangeResults {
    add: Option<usize>,
    remove: Option<usize>,
}

pub trait Index<K, V> {
    fn get(&mut self, k: &K) -> PERes<Option<Value<V>>>;
    fn iter_from(&mut self, first: Bound<&K>) -> PERes<PageIter<K, V>>;
    fn back_iter_from(&mut self, first: Bound<&K>) -> PERes<PageIterBack<K, V>>;
}

pub(crate) trait IndexApply<K, V>: Index<K, V> {
    fn apply(&mut self, adds: &[KeyChanges<K, V>]) -> PIRes<()>;
}
impl<K: Display> Display for PosRef<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}=>{}", self.k, self.pos, self.node_ref)
    }
}

#[derive(PartialEq, Clone, Debug, Eq)]
pub struct PosRef<K> {
    pub k: K,
    pub pos: usize,
    pub node_ref: TreeNodeRef,
}

impl<K: Clone> PosRef<K> {
    pub(crate) fn new(k: &K, pos: usize, node_ref: TreeNodeRef) -> PosRef<K> {
        PosRef {
            k: k.clone(),
            pos,
            node_ref,
        }
    }
}

fn split_and_save<K, V, I>(
    index: &mut I,
    id: TreeNodeRef,
    node: Rc<TreeNode<K, V>>,
    version: u16,
) -> PIRes<Vec<(K, TreeNodeRef)>>
where
    K: IndexOrd + Clone,
    I: IndexModify<K, V>,
{
    let (mut first, _) = index.owned(&id, node);
    let new_nodes = first.split(index.limits().top());
    let mut ids = Vec::new();
    for (k, new_node) in new_nodes {
        let saved_id = index.insert(new_node)?;
        ids.push((k, saved_id));
    }
    index.update(&id, first, version)?;
    Ok(ids)
}

fn recompute_path<K, V, I>(index: &mut I, child: &ChildChanged<K>) -> PIRes<Path<K>>
where
    K: IndexOrd + Clone,
    I: IndexModify<K, V>,
{
    let node = index.get_root_refresh()?.expect("Root is there");
    let mut path = Path::new(child.search_key().clone());
    let mut cur_node = PosRef::new(child.search_key(), 0, node);
    loop {
        let mut restart = true;
        if let Some((node_modify, version)) = index.load_modify(&cur_node.node_ref)? {
            path.push(cur_node.clone(), version, node_modify.len());
            if cur_node.node_ref == child.item().pos_ref().node_ref {
                path.compute_depths_start(child.item().bottom_depth());
                path.replace_search_key(cur_node.k);
                break Ok(path);
            }
            if let TreeNode::Node(n) = &*node_modify {
                if let Some(x) = n.find_write(child.search_key()) {
                    cur_node = x;
                    restart = false;
                }
            }
        }
        if restart {
            let node = index.get_root_refresh()?.expect("Root is there");
            cur_node = PosRef::new(child.search_key(), 0, node);
            path.clear();
        }
    }
}

fn recompute_path_depth<K, V, I>(index: &mut I, depth: BottomDepth, k: &K) -> PIRes<Path<K>>
where
    K: IndexOrd + Clone,
    I: IndexModify<K, V>,
{
    let node = index.get_root_refresh()?.expect("Root is there");
    let mut path = Path::new(k.clone());
    let mut cur_node = PosRef::new(k, 0, node);
    loop {
        let mut restart = true;
        if let Some((node_modify, version)) = index.load_modify(&cur_node.node_ref)? {
            path.push(cur_node.clone(), version, node_modify.len());
            match &*node_modify {
                TreeNode::Node(n) => {
                    if let Some(x) = n.find_write(k) {
                        cur_node = x;
                        restart = false;
                    }
                }
                TreeNode::Leaf(_n) => {
                    path.compute_depths();
                    break;
                }
            }
        }
        if restart {
            let node = index.get_root_refresh()?.expect("Root is there");
            cur_node = PosRef::new(k, 0, node);
            path.clear();
        }
    }
    path.short_to_depth(depth);
    Ok(path)
}

fn recompute_path_depth_pre<K, V, I>(index: &mut I, depth: BottomDepth, k: &K) -> PIRes<Path<K>>
where
    K: IndexOrd + Clone,
    I: IndexModify<K, V>,
{
    let node = index.get_root_refresh()?.expect("Root is there");
    let mut path = Path::new(k.clone());
    let mut cur_node = PosRef::new(k, 0, node);
    loop {
        let mut restart = true;
        if let Some((node_modify, version)) = index.load_modify(&cur_node.node_ref)? {
            path.push(cur_node.clone(), version, node_modify.len());
            match &*node_modify {
                TreeNode::Node(n) => {
                    if let Some(x) = n.find_pre_write(k) {
                        cur_node = x;
                        restart = false;
                    }
                }
                TreeNode::Leaf(_n) => {
                    path.replace_search_key(cur_node.k.clone());
                    path.compute_depths();
                    break;
                }
            }
        }
        if restart {
            let node = index.get_root_refresh()?.expect("Root is there");
            cur_node = PosRef::new(k, 0, node);
            path.clear();
        }
    }
    path.short_to_depth(depth);
    Ok(path)
}

fn lock_parents<K, V, I>(
    index: &mut I,
    updates: Vec<LockGroup<K>>,
) -> PIRes<(Vec<LockGroup<K>>, Vec<Vec<ToMergeCheck<K>>>)>
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
    I: IndexModify<K, V>,
{
    let mut group_changes = LockGroups::new();
    let mut checks_groups = Vec::new();
    for update in &updates {
        let mut parent_group = update.parent_group(index.limits());
        let mut group_len = parent_group.len();
        let mut cur = 0;
        let mut checks = Vec::new();
        let mut retry = 0;
        while cur < group_len {
            let recompute = {
                let parent = &mut parent_group[cur];

                if !parent.to_lock() {
                    cur += 1;
                    continue;
                }
                let last_pos = parent.path().last().unwrap();
                let pos_ref = last_pos.pos_ref();
                let node_ref = pos_ref.node_ref;
                if let Some((node, version)) = index.load_modify(&node_ref)? {
                    if last_pos.version() == version {
                        let path = parent.path().clone();
                        if lock_logic(index, path, node, &mut group_changes, Some(parent))? {
                            checks.extend(parent.children_checks());
                            cur += 1;
                            retry = 0;
                            false
                        } else {
                            true
                        }
                    } else {
                        true
                    }
                } else {
                    true
                }
            };

            if recompute {
                retry += 1;
                if retry > 1000000 {
                    return Err(crate::IndexChangeError::ReachedLimitOfRetry);
                }
                let mut children = Vec::<ChildChanged<K>>::new();
                let mut inserted = 0;
                let mut prev_path: Option<Path<K>> = None;
                let old_children = parent_group[cur].children().clone();
                for mut child in old_children {
                    let mut path = recompute_path(index, &child)?;
                    child.replace_item(path.pop().unwrap());
                    if let Some(pp) = prev_path {
                        if pp.last_id() != path.last_id() {
                            if inserted == 0 {
                                parent_group[cur].replace(pp.clone(), children);
                            } else {
                                let element = ParentChange::new_childs(pp.clone(), children);
                                group_len += 1;
                                parent_group.insert(cur + inserted, element);
                            }
                            inserted += 1;
                            children = Vec::new();
                        }
                    }
                    prev_path = Some(path);
                    children.push(child);
                }
                if !children.is_empty() {
                    if let Some(pp) = prev_path {
                        if inserted == 0 {
                            parent_group[cur].replace(pp, children);
                        } else {
                            let element = ParentChange::new_childs(pp, children);
                            group_len += 1;
                            parent_group.insert(cur + inserted, element);
                        }
                    }
                }
            }
        }
        if !checks.is_empty() {
            checks_groups.push(checks);
        }
    }
    group_changes.finish();
    Ok((group_changes.groups(), checks_groups))
}

#[derive(Clone)]
pub(crate) struct ToMergeCheck<K> {
    parent: PosRef<K>,
    child: PosRef<K>,
    check_key: bool,
}

impl<K> ToMergeCheck<K> {
    fn new(parent: PosRef<K>, child: PosRef<K>, check_key: bool) -> Self {
        Self {
            parent,
            child,
            check_key,
        }
    }
}

fn rebalance<K, V, I>(index: &mut I, to_balance: Vec<ToMergeCheck<K>>) -> PIRes<()>
where
    K: IndexOrd + Clone + Display,
    I: IndexModify<K, V>,
    V: Display,
{
    let mut prev: Option<(ToMergeCheck<K>, Rc<TreeNode<K, V>>, u16)> = None;
    for rmc in &to_balance {
        let pos = &rmc.child;
        if let Some((node, version)) = index.load_modify(&pos.node_ref)? {
            if let Some((prev_id, prev_node, prev_version)) = prev {
                if node.len() < index.limits().bottom() || prev_node.len() < index.limits().bottom() {
                    let (mut dest_merge, _) = index.owned(&prev_id.child.node_ref, prev_node);
                    let (mut source_merge, _) = index.owned(&pos.node_ref, node);
                    let merge_key = source_merge.get_prev().as_ref().unwrap().clone();
                    //let parent_key = &pos.k;
                    dest_merge.merge_right(&merge_key, &mut source_merge);
                    let parent_id = rmc.parent.node_ref;

                    let (parent_node, parent_version) = index.load_modify(&parent_id)?.unwrap();
                    let mut parent = index.owned(&parent_id, parent_node).0.as_node();
                    let parent_key = parent.find_key(&pos.node_ref).unwrap().clone();
                    let next_to_set = parent.remove_key(&parent_key);
                    index.update(&parent_id, TreeNode::Node(parent), parent_version)?;

                    if let Some(next) = next_to_set {
                        let prev_parent_id = prev_id.parent.node_ref;
                        let (prev_parent_node, prev_parent_version) = index.load_modify(&prev_parent_id)?.unwrap();
                        let mut prev_parent = index.owned(&prev_parent_id, prev_parent_node).0.as_node();
                        prev_parent.swap_next(&merge_key, &next);
                        index.update(&prev_parent_id, TreeNode::Node(prev_parent), prev_parent_version)?;
                    }
                    index.delete(&pos.node_ref, version)?;
                    index.update(&prev_id.child.node_ref, dest_merge, prev_version)?;
                    let (node, version) = index.load_modify(&prev_id.child.node_ref)?.unwrap();
                    prev = Some((prev_id, node, version));
                } else if rmc.check_key {
                    let parent_id = rmc.parent.node_ref;
                    let (parent_node, parent_version) = index.load_modify(&parent_id)?.unwrap();

                    let old_key = if let TreeNode::Node(parent_node_n) = &*parent_node {
                        parent_node_n.find_key(&pos.node_ref)
                    } else {
                        unreachable!()
                    }
                    .unwrap()
                    .clone();
                    let new_key = node.get_prev().as_ref().unwrap();
                    if compare(&old_key, new_key) != Ordering::Equal {
                        let (need_change, need_change_prev_next) = if let TreeNode::Node(path_parent_n) = &*parent_node
                        {
                            path_parent_n.need_swap_key(&old_key)
                        } else {
                            (false, false)
                        };
                        if need_change || need_change_prev_next {
                            let mut parent = index.owned(&parent_id, parent_node).0.as_node();
                            parent.swap_key(&old_key, new_key);
                            index.update(&parent_id, TreeNode::Node(parent), parent_version)?;
                        }

                        let prev_parent_id = prev_id.parent.node_ref;
                        let (prev_parent_node, prev_parent_version) = index.load_modify(&prev_parent_id)?.unwrap();

                        let need_change_next = if let TreeNode::Node(path_parent_n) = &*prev_parent_node {
                            path_parent_n.need_swap_next(&old_key)
                        } else {
                            false
                        };
                        debug_assert_eq!(need_change_prev_next, need_change_next);
                        if need_change_next {
                            let mut prev_parent = index.owned(&prev_parent_id, prev_parent_node).0.as_node();
                            prev_parent.swap_next(&old_key, new_key);
                            index.update(&prev_parent_id, TreeNode::Node(prev_parent), prev_parent_version)?;
                        }
                    }
                    prev = Some((rmc.clone(), node, version));
                } else {
                    prev = Some((rmc.clone(), node, version));
                }
            } else {
                prev = Some((rmc.clone(), node, version));
            }
        }
    }
    std::mem::drop(prev);
    if to_balance.len() == 1 {
        let tb = &to_balance[0];
        if tb.check_key {
            let parent_id = tb.parent.node_ref;
            let (parent_node, parent_version) = index.load_modify(&parent_id)?.unwrap();

            let old_key = if let TreeNode::Node(parent_node_n) = &*parent_node {
                parent_node_n.find_key(&tb.child.node_ref)
            } else {
                unreachable!()
            }
            .unwrap()
            .clone();
            let (node, _version) = index.load_modify(&tb.child.node_ref)?.unwrap();
            let new_key = node.get_prev().as_ref().unwrap();
            if compare(&old_key, new_key) != Ordering::Equal {
                let (need_change, need_change_prev_next) = if let TreeNode::Node(path_parent_n) = &*parent_node {
                    path_parent_n.need_swap_key(&old_key)
                } else {
                    (false, false)
                };
                if need_change || need_change_prev_next {
                    let mut parent = index.owned(&parent_id, parent_node).0.as_node();
                    parent.swap_key(&old_key, new_key);
                    index.update(&parent_id, TreeNode::Node(parent), parent_version)?;
                }
                debug_assert!(!need_change_prev_next);
            }
        }
    }

    for rmc in &to_balance {
        let pos = &rmc.child;

        if let Some((node, version)) = index.load_modify(&pos.node_ref)? {
            if node.len() > index.limits().top() {
                let (mut first, _) = index.owned(&pos.node_ref, node);
                let new_nodes = first.split(index.limits().top());
                let mut ids = Vec::new();
                for (k, new_node) in new_nodes {
                    let saved_id = index.insert(new_node)?;
                    ids.push((k, saved_id));
                }
                index.update(&pos.node_ref, first, version)?;

                let rp = rmc.parent.node_ref;
                let (parent_node, parent_version) = index.load_modify(&rp)?.unwrap();
                let mut parent = index.owned(&rp, parent_node).0.as_node();
                parent.insert_after_key(&ids);
                index.update(&rp, TreeNode::Node(parent), parent_version)?;
            }
        }
    }
    Ok(())
}

struct ToLock<K> {
    prev: Option<K>,
    next: Option<K>,
    len: usize,
    path: Path<K>,
}

struct ToLockNodes<K> {
    to_lock: Vec<ToLock<K>>,
}
impl<K: IndexOrd + Clone> ToLockNodes<K> {
    fn new() -> Self {
        Self {
            to_lock: Default::default(),
        }
    }
    fn push<V>(&mut self, node: &Rc<TreeNode<K, V>>, path: Path<K>) {
        debug_assert!(path.last_bottom_depth().is_some());
        self.to_lock.push(ToLock {
            prev: node.get_prev().clone(),
            next: node.get_next().clone(),
            len: node.len(),
            path,
        });
    }
    fn are_one_after_the_other(&self) -> bool {
        let mut prev_next: Option<Option<K>> = None;
        for to_lock in &self.to_lock {
            if let Some(pr) = prev_next {
                if let (Some(f), Some(s)) = (pr, &to_lock.prev) {
                    if f.cmp(s) != Ordering::Equal {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            prev_next = Some(to_lock.next.clone());
        }
        true
    }
    fn lock_all<I: IndexModify<K, V>, V>(&self, index: &mut I) -> bool {
        let mut locked = Vec::new();
        for to_lock in &self.to_lock {
            let last = to_lock.path.last().unwrap();
            if !index.lock(&last.pos_ref().node_ref, last.version()).unwrap_or(false) {
                for to_unlock in locked {
                    index.unlock(&to_unlock);
                }
                return false;
            }
            locked.push(last.pos_ref().node_ref);
        }
        true
    }
    fn len(&self) -> usize {
        self.to_lock.len()
    }
}

/// lock the node and a sibling node that may be target by a merge with the current node.
fn lock_logic<K, V, I>(
    index: &mut I,
    path: Path<K>,
    node: Rc<TreeNode<K, V>>,
    lock_groups: &mut LockGroups<K>,
    pc: Option<&ParentChange<K>>,
) -> PIRes<bool>
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
    I: IndexModify<K, V>,
{
    let mut bottom_limit = index.limits().bottom() as isize;
    let top_depth = path.depth();
    let cur_item = path.last().unwrap();
    let cur_node = cur_item.pos_ref();
    if !node.check_range(path.search_key()) {
        return Ok(false);
    }

    let bottom_depth = path.last_bottom_depth().unwrap();

    if top_depth > TopDepth(0) {
        let parent_item = path.get(top_depth - TopDepth(1)).unwrap();
        let parent = parent_item.pos_ref();
        let mut to_lock = ToLockNodes::new();
        if let Some((n, version)) = index.load_modify(&parent.node_ref)? {
            if parent_item.version() != version {
                return Ok(false);
            }
            if let TreeNode::Node(parent_node) = &*n {
                let mut len = node.len() as isize;
                if let Some(parent_change) = pc {
                    len += parent_change.add_count() as isize;
                    len -= parent_change.remove_count() as isize;
                    if parent_change.change_prev_key() {
                        if let Some(key) = node.get_prev() {
                            let pre_path = recompute_path_depth_pre(index, bottom_depth, key)?;
                            let pr = pre_path.last_pos_ref().unwrap();
                            if let Some((n, _v)) = index.load_modify(&pr.node_ref)? {
                                let len = n.len() as isize;
                                to_lock.push(&n, pre_path);
                                bottom_limit -= len;
                            } else {
                                return Ok(false);
                            }
                        }
                    }
                }
                bottom_limit -= len;

                to_lock.push(&node, path.clone());
                let iter = if let Some(iter) = parent_node.iter_from(Some(cur_node)) {
                    iter
                } else {
                    return Ok(false);
                };
                for next_pos in iter {
                    if let Some((n, v)) = index.load_modify(&next_pos.node_ref)? {
                        let len = n.len() as isize;
                        let mut new_path = path.clone();
                        new_path.pop();
                        new_path.replace_search_key(next_pos.k.clone());
                        new_path.push(next_pos, v, n.len());
                        new_path.compute_depths_start(bottom_depth);
                        to_lock.push(&n, new_path);
                        bottom_limit -= len;
                    } else {
                        return Ok(false);
                    }
                    if bottom_limit <= 0 {
                        break;
                    }
                }

                let mut next_key = parent_node.get_next().clone();
                while (bottom_limit > 0 || to_lock.len() < 2) && next_key.is_some() {
                    if let Some(key) = next_key.take() {
                        let mut parent_path = recompute_path_depth(index, bottom_depth, &key)?;
                        parent_path.pop();
                        let parent_id = &parent_path.last_pos_ref().unwrap();
                        if let Some((p, _)) = index.load_modify(&parent_id.node_ref)? {
                            if let TreeNode::Node(parent) = &*p {
                                let iter = if let Some(iter) = parent.iter_from(None) {
                                    iter
                                } else {
                                    return Ok(false);
                                };
                                for next_pos in iter {
                                    if let Some((n, v)) = index.load_modify(&next_pos.node_ref)? {
                                        let len = n.len() as isize;
                                        let mut new_path = parent_path.clone();
                                        new_path.replace_search_key(next_pos.k.clone());
                                        new_path.push(next_pos, v, n.len());
                                        new_path.compute_depths_start(bottom_depth);
                                        to_lock.push(&n, new_path);
                                        bottom_limit -= len;
                                    } else {
                                        return Ok(false);
                                    }
                                    if bottom_limit <= 0 {
                                        break;
                                    }
                                }
                                next_key = p.get_next().clone();
                            }
                        }
                    }
                }
            } else {
                return Ok(false);
            }
        } else {
            return Ok(false);
        };

        if !to_lock.are_one_after_the_other() {
            return Ok(false);
        }
        if !to_lock.lock_all(index) {
            return Ok(false);
        }
        let first_path = &to_lock.to_lock.first().unwrap().path;
        let lock_group = lock_groups.group_for_id(first_path.last_pos_ref().unwrap(), bottom_depth);
        for to_lock in to_lock.to_lock {
            lock_group.add(to_lock.path, to_lock.len, &pc);
        }
        Ok(true)
    } else if index.lock(&cur_node.node_ref, cur_item.version())? {
        let lock_group = lock_groups.group_for_id(cur_node, bottom_depth);
        lock_group.add(path, node.len(), &pc);
        Ok(true)
    } else {
        Ok(false)
    }
}

fn find_and_change_leaf<K, V, I>(change: &KeyChanges<K, V>, updates: &mut LockGroups<K>, index: &mut I) -> PIRes<()>
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
    I: IndexModify<K, V>,
{
    let mut next: Option<PosRef<K>> = None;
    let mut path = Path::new(change.k.clone());
    let mut retry_count = 0;
    loop {
        next = if let Some(cur_node) = &mut next {
            if let Some((node_modify, version)) = index.load_modify(&cur_node.node_ref)? {
                match &*node_modify {
                    TreeNode::Node(n) => {
                        path.push(cur_node.clone(), version, node_modify.len());
                        n.find_write(&change.k)
                    }
                    TreeNode::Leaf(ref _none) => {
                        let node_ref = &cur_node.node_ref;
                        path.push(cur_node.clone(), version, node_modify.len());
                        path.compute_depths();

                        let locked = lock_logic(index, path, node_modify.clone(), updates, None)?;
                        if locked {
                            let (loaded, already_changed) = index.owned(&cur_node.node_ref, node_modify);
                            let mut leaf = loaded.as_leaf();
                            let cr = change.apply(&mut leaf, index.value_mode(), index.index_name())?;
                            if cr.add.is_some() || cr.remove.is_some() {
                                updates.last_set_change(&cur_node.node_ref, cr);
                                index.update(node_ref, TreeNode::Leaf(leaf), version)?;
                            } else if already_changed {
                                index.update(node_ref, TreeNode::Leaf(leaf), version)?;
                            }
                            break;
                        } else {
                            path = Path::new(change.k.clone());
                            None
                        }
                    }
                }
            } else {
                None
            }
        } else if let Some(r) = index.get_root_refresh()? {
            retry_count += 1;
            if retry_count > 1000000 {
                return Err(crate::IndexChangeError::ReachedLimitOfRetry);
            }
            path.clear();
            Some(PosRef::new(&change.k, 0, r))
        } else {
            retry_count += 1;
            if retry_count > 1000000 {
                return Err(crate::IndexChangeError::ReachedLimitOfRetry);
            }
            path.clear();
            index.lock_config()?;
            if let Some(r) = index.get_root_refresh()? {
                Some(PosRef::new(&change.k, 0, r))
            } else {
                let mut leaf = Leaf::new();
                change.apply(&mut leaf, index.value_mode(), index.index_name())?;
                let leaf_ref = index.insert(TreeNode::Leaf(leaf))?;
                index.set_root(Some(leaf_ref))?;
                break;
            }
        }
    }
    Ok(())
}

impl<K, V, T> IndexApply<K, V> for T
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
    T: IndexModify<K, V>,
    T: Index<K, V>,
{
    fn apply(&mut self, adds: &[KeyChanges<K, V>]) -> PIRes<()> {
        let mut changes_group = LockGroups::new();

        for add in adds {
            find_and_change_leaf(add, &mut changes_group, self)?;
        }
        changes_group.finish();

        let mut updates = changes_group.groups();

        let mut merge_ands_save = Vec::new();
        while updates.len() > 1 || (updates.len() == 1 && updates[0].is_not_only_root()) {
            let (parent_updates, parent_changes) = lock_parents(self, updates)?;

            merge_ands_save.push(parent_changes);
            updates = parent_updates;
        }

        for parent_changes in merge_ands_save {
            for update in parent_changes {
                // It's locked, should not have miss read unwrap.
                rebalance(self, update)?;
            }
        }
        if updates.len() == 1 && updates[0].is_only_root() {
            self.lock_config()?;
            while let Some(r) = self.get_root_refresh()? {
                if let Some((n, n_version)) = self.load_modify(&r)? {
                    if n.len() > self.limits().top() {
                        let ids = split_and_save(self, r, n, n_version)?;
                        let node = TreeNode::Node(Node::new_from_split(r, &ids));
                        let cur_id = self.insert(node)?;
                        self.set_root(Some(cur_id))?;
                    } else if n.len() == 1 {
                        if let TreeNode::Node(cn) = &*n {
                            self.delete(&r, n_version)?;
                            self.set_root(Some(cn.get(0)))?;
                        } else {
                            break;
                        }
                    } else if n.len() == 0 {
                        self.set_root(None)?;
                    } else {
                        break;
                    }
                }
            }
        }
        Ok(())
    }
}

impl<K, V, T> Index<K, V> for T
where
    K: IndexTypeInternal,
    V: IndexTypeInternal,
    T: IndexKeeper<K, V>,
{
    fn get(&mut self, k: &K) -> PERes<Option<Value<V>>> {
        Ok(if let Some(node) = self.get_root()? {
            let mut cur_node = node;
            let mut reuse = None;
            loop {
                match self.load_with(&cur_node, reuse)? {
                    TreeNode::Node(n) => {
                        cur_node = n.find(k).node_ref;
                        reuse = Some(n);
                    }
                    TreeNode::Leaf(leaf) => {
                        break leaf.find(k).map(|el| el.1).ok();
                    }
                }
            }
        } else {
            None
        })
    }

    fn iter_from(&mut self, first: Bound<&K>) -> PERes<PageIter<K, V>> {
        let mut path = Vec::new();
        let mut iter = if let Some(mut cur_node) = self.get_root()? {
            path.push((0, cur_node));
            let mut reuse = None;
            loop {
                match self.load_with(&cur_node, reuse)? {
                    TreeNode::Node(n) => {
                        let value = match first {
                            Bound::Included(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Excluded(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Unbounded => (0, n.get(0)),
                        };
                        cur_node = value.1;
                        path.push(value);
                        reuse = Some(n);
                    }
                    TreeNode::Leaf(leaf) => {
                        break PageIter {
                            iter: leaf.iter_from(first).peekable(),
                        };
                    }
                }
            }
        } else {
            PageIter {
                iter: Vec::new().into_iter().peekable(),
            }
        };

        while iter.iter.peek().is_none() {
            let prev = path.pop();
            if let Some((_, node)) = path.last() {
                let (pos, _) = prev.unwrap();
                match self.load(node)? {
                    TreeNode::Node(n) => {
                        // check if there are more elements in the node
                        if n.len() > pos + 1 {
                            let mut cur_node = n.get(pos + 1);
                            loop {
                                match self.load(&cur_node)? {
                                    TreeNode::Node(n) => {
                                        cur_node = n.get(0);
                                    }
                                    TreeNode::Leaf(leaf) => {
                                        // Use here the key anyway, should start from the first in
                                        // any case
                                        iter = PageIter {
                                            iter: leaf.iter_from(first).peekable(),
                                        };
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    TreeNode::Leaf(_leaf) => {
                        panic!("can't happen");
                    }
                }
            } else {
                break;
            }
        }
        Ok(iter)
    }

    fn back_iter_from(&mut self, last: Bound<&K>) -> PERes<PageIterBack<K, V>> {
        let mut path = Vec::new();
        let mut iter = if let Some(mut cur_node) = self.get_root()? {
            path.push((0, cur_node));
            let mut reuse = None;
            loop {
                match self.load_with(&cur_node, reuse)? {
                    TreeNode::Node(n) => {
                        let value = match last {
                            Bound::Included(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Excluded(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Unbounded => {
                                let pos = n.len() - 1;
                                (pos, n.get(pos))
                            }
                        };
                        cur_node = value.1;
                        path.push(value);
                        reuse = Some(n);
                    }
                    TreeNode::Leaf(leaf) => {
                        break PageIterBack {
                            iter: leaf.back_iter_from(last).peekable(),
                        };
                    }
                }
            }
        } else {
            PageIterBack {
                iter: Vec::new().into_iter().rev().peekable(),
            }
        };

        while iter.iter.peek().is_none() {
            let prev = path.pop();
            if let Some((_, node)) = path.last() {
                let (pos, _) = prev.unwrap();
                match self.load(node)? {
                    TreeNode::Node(n) => {
                        // check if there are more elements in the node
                        if pos > 0 {
                            let mut cur_node = n.get(pos - 1);
                            loop {
                                match self.load(&cur_node)? {
                                    TreeNode::Node(n) => {
                                        cur_node = n.get(n.len() - 1);
                                    }
                                    TreeNode::Leaf(leaf) => {
                                        // Use here the key anyway, should start from the first in
                                        // any case
                                        iter = PageIterBack {
                                            iter: leaf.back_iter_from(last).peekable(),
                                        };
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    TreeNode::Leaf(_leaf) => {
                        panic!("can't happen");
                    }
                }
            } else {
                break;
            }
        }
        Ok(iter)
    }
}
